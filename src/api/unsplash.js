import axios from 'axios';

export default axios.create({
    baseURL: 'https://api.unsplash.com',
    headers: {
        Authorization: 'Client-ID yxLtpl22qsD1kXfOEeLkes50g4P0uBcKFhHZ_Q-aYVI'
    }
});