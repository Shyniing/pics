## TP Heroku
Clément Pouilloux

# Différentes étapes

* Créations de différents projet sur Heroku
* Création du projet sur git lab
* Ajout du fichier gitlab-ci.yml
* Envoie de tous sur la branche recette
* Merge de recette vers develop
* Merge de develop vers master
* Ajout du dockerfile
* Modification du gitlab-ci pour le lancement du container docker
